# Laravel core app resources

Laravel module created for [nwidart/laravel-modules](https://github.com/nWidart/laravel-modules) into the Modules/ directory.

## Install

## Usage

Add to `app/Http/Kernel.php`:
```php
    protected $middlewareGroups = [
        'web' => [
            //...
            \Modules\AppCore\Http\Middleware\SaveRequestUserOptions::class,
        ],
```

## Credits
- [nwidart/laravel-modules](https://github.com/nWidart/laravel-modules)
- [Ducumentation Laravel-Modules](https://nwidart.com/laravel-modules)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.